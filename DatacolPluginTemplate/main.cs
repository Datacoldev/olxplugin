﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Collections.Specialized;
using System.IO;
using LowLevel;
using System.Data;
using System.Net;
using System.Threading;
using DatacolChrome;
using OpenQA.Selenium;

namespace Plugin
{
    /// <summary>
    /// Простейшая реализация интерфейса плагина, сборку с этим классом мы будем подгружать динамически,
    /// главный проект не имеет на нее ссылок! Класс реализует интерфейс плагина, 
    /// для унифицированной работы со всеми плагинами
    /// а так же для того, чтобы можно было динамически найти класс в сборке.
    /// </summary>
    public class HandlerClass : PluginInterface.IPlugin
    {
        public volatile int globalLoadCounter = 0;

        /// <summary>
        /// Обработчик плагина
        /// </summary>
        /// <param name="parameters">Словарь параметров: ключ - имя параметра (string), 
        /// значение - содержимое параметра (object, который в зависимости от типа плагина (задается в parameters["type"])
        /// и ключа приводится к тому или иному типу) </param>
        /// <param name="error">Переменная (string), в которую возвращается ошибка работы плагина, 
        /// если таковая произошла. Если ошибки не произошло, данная переменная должна оставаться пустой строкой</param>
        /// <returns>Возвращаемое значение - это объект, который может иметь тот или иной тип,
        /// в зависимости от типа плагина (задается в  parameters["type"])</returns>
        public object pluginHandler(Dictionary<string, object> parameters, out string error)
        {
            try
            {
                error = "";
                string campaignname = parameters["campaignname"].ToString();
                string type = parameters["type"].ToString();

                #region starting_urls_plugin (плагин обработки начальных URL)
                if (extra.sc(type, "starting_urls_plugin"))
                {
                    //параметр список начальных URL
                    List<string> startingurls = (List<string>)parameters["startingurls"];
                    //параметр путь к файлу с начальными URL
                    string startingurlsfile = parameters["startingurlsfile"].ToString();

                    List<string> resultingList = new List<string>();

                    foreach (string u in startingurls)
                    {
                        resultingList.Add(u + "_plug");
                    }

                    //возвращает обработанный (полученный) список начальных URL
                    return resultingList;
                }
                #endregion

                #region load_page_plugin (плагин загрузки страницы)
                if (extra.sc(type, "load_page_plugin"))
                {
                    //параметр ССЫЛКА на загружаемую страницу
                    string url = parameters["url"].ToString();
                    //параметр уровень вложенности загружаемой страницы
                    int nestinglevel = Convert.ToInt32(parameters["nestinglevel"].ToString());
                    //параметр реферер для загружаемой страницы
                    string referer = parameters["referer"].ToString();
                    //параметр флаг использования прокси при загрузке
                    bool useproxy = Convert.ToBoolean(parameters["useproxy"].ToString());
                    //параметр объект Загрузчик Datacol
                    DatacolHttp http = (DatacolHttp)parameters["datacolhttp"];
                    //параметр имя прокси чекера
                    string checkername = parameters["checkername"].ToString();
                    //параметр режим использования прокси (список или из прокси чекера)
                    string proxymode = parameters["proxymode"].ToString();
                    //параметр предопределенный прокси для загрузки страницы
                    WebProxy webproxy = (WebProxy)parameters["webproxy"];
                    //параметр предопределенная кодировка загружаемой страницы
                    string encoding = parameters["encoding"].ToString();
                    WebProxy usedProxy = new WebProxy();
                    string config = "";


                    #region <scriptparam>

                    string script_param = extra.gra(url, "<script_param>([^<>]*?)</script_param>");

                    url = Regex.Replace(url, "<script_param>[^<>]*?</script_param>", "", RegexOptions.IgnoreCase | RegexOptions.Singleline);
                    #endregion




                    if (parameters.ContainsKey("config"))
                    {
                        config = parameters["config"].ToString();
                    }

                    string index = "";
                    string content = "";

                    #region Initialize browser

                    #region plugin specific params

                    string dllpath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    //string filename = Path.Combine(dllpath,"D5Plugin_Chrome.txt");
                    //Dictionary<string, object> configParams = GetDictionaryParams(filename);
                    Dictionary<string, object> configParams = GetDictionaryParamsConfig(config);

                    string give_control_to_user_xpath = "";
                    List<string> valids = new List<string>();
                    List<string> invalids = new List<string>();

                    List<string> invalid_load_strings = new List<string>();
                    bool disableImages = false;
                    string nonBrowserLoadRegex = "";
                    bool disableRegion = false;
                    bool showbrowser = false;
                    if (configParams.ContainsKey("give_control_to_user_xpath"))
                    {
                        give_control_to_user_xpath = configParams["give_control_to_user_xpath"].ToString();
                    }

                    if (configParams.ContainsKey("disable_images"))
                    {
                        disableImages = Convert.ToInt32(configParams["disable_images"].ToString()) == 1;
                    }


                    if (configParams.ContainsKey("invalid_load_strings"))
                    {
                        invalid_load_strings = (List<string>)configParams["invalid_load_strings"];
                    }

                    if (configParams.ContainsKey("non_browser_load_regex"))
                    {
                        nonBrowserLoadRegex = configParams["non_browser_load_regex"].ToString();
                    }

                    if (configParams.ContainsKey("disable_region"))
                    {
                        disableRegion = Convert.ToInt32(configParams["disable_region"].ToString()) == 1;
                    }
                    if (configParams.ContainsKey("show_browser"))
                    {
                        showbrowser = Convert.ToInt32(configParams["show_browser"].ToString()) == 1;
                    }


                    valids = http.ValidRegexes;
                    invalids = http.InvalidRegexes;

                    /*if (configParams.ContainsKey("valids"))
                    {
                        valids = extra.GetAllLines(configParams["valids"].ToString(),true);
                    }

                    if (configParams.ContainsKey("invalids"))
                    {
                        invalids = extra.GetAllLines(configParams["invalids"].ToString(), true);
                    }*/
                    #endregion




                    if (nonBrowserLoadRegex != "" && Regex.IsMatch(url, nonBrowserLoadRegex,
                        RegexOptions.Singleline | RegexOptions.IgnoreCase))
                    {
                        Dictionary<string, object> outDictParams = new Dictionary<string, object>();

                        return http.request(url, url, out outDictParams, out error);
                    }

                    #region basic params
                    configParams.Add("useproxy", useproxy);

                    configParams.Add("predefinedwebproxy", webproxy);
                    configParams.Add("proxymode", proxymode);
                    configParams.Add("datacolhttpproxylist", http.ProxyList);
                    #endregion

                    configParams.Add("currentURL", url);

                    s browser = bundle.iI(dllpath, campaignname, configParams, out error, out index, invalid_load_strings, disableImages, disableRegion, showbrowser);

                    if (configParams.ContainsKey("request_per_proxy"))
                    {
                        browser.ReqPerProxy = Convert.ToInt32(configParams["request_per_proxy"].ToString());
                    }
                    if (configParams.ContainsKey("incognito"))
                    {
                        browser.incognito = Convert.ToInt32(configParams["incognito"].ToString()) == 1;
                    }

                    if (error != "") return "";

                    #endregion

                    browser.n(url, out content, out error);





                    if (error != "") return "";

                    #region give user control
                    if (give_control_to_user_xpath != "")
                    {
                        if (browser.eE(give_control_to_user_xpath))
                        {
                            datacol_help_forms.form_interface fi = new datacol_help_forms.form_interface();
                            fi.showDialog();

                        }
                    }
                    #endregion

                    #region scenario processing

                    string scenario = extra.gra(config,
                        "<scenario>(.*?)</scenario>");

                    List<string> commandBlocks = extra.matches(config, "<cblock>(.*?)</cblock>", 1);

                    for (int i = 0; i < commandBlocks.Count; i++)
                    {
                        string condition = extra.gra(commandBlocks[i], "<condition>(.*?)</condition>");

                        if (condition != "")
                        {
                            string xpath = extra.gra(condition, "<cparam name='xpath'>(.*?)</cparam>");
                            string regex = extra.gra(condition, "<cparam name='regex'>(.*?)</cparam>");

                            string conditionType = extra.gra(condition, "<ctype>(.*?)</ctype>");

                            bool cont = true;
                            switch (conditionType)
                            {

                                case "elementExists":
                                    if (browser.eE(xpath))
                                    {
                                        cont = false;
                                    }
                                    break;
                                case "regexExists":
                                    if (Regex.IsMatch(browser.gPC(out error), regex, RegexOptions.Singleline | RegexOptions.IgnoreCase))
                                    {
                                        cont = false;
                                    }
                                    break;
                                case "elementNotExists":
                                    if (!browser.eE(xpath))
                                    {
                                        cont = false;
                                    }
                                    break;
                                case "regexNotExists":
                                    if (!Regex.IsMatch(browser.gPC(out error), regex, RegexOptions.Singleline | RegexOptions.IgnoreCase))
                                    {
                                        cont = false;
                                    }
                                    break;
                                default:
                                    throw new Exception("Тип условия " + conditionType + " не поддерживается");
                                    break;
                            }

                            //если не удовлетворяет условию
                            if (cont) continue;
                        }

                        List<string> commands = extra.matches(commandBlocks[i], "<command>(.*?)</command>");

                        foreach (string command in commands)
                        {
                            bool detectErrors = extra.gra(command, "(<detectErrors>)") != "";
                            string xpath = extra.gra(command, "<param name='xpath'>(.*?)</param>");
                            string id = extra.gra(command, "<param name='id'>(.*?)</param>");
                            string regex = extra.gra(command, "<param name='regex'>(.*?)</param>");
                            string script = extra.gra(command, "<param name='script'>(.*?)</param>");
                            string text = extra.gra(command, "<param name='text'>(.*?)</param>");
                            string picturename = extra.gra(command, "<param name='picturename'>(.*?)</param>");
                            int count = 1;
                            try
                            {
                                count = Convert.ToInt32(extra.gra(command, "<param name='count'>(.*?)</param>"));
                            }
                            catch (Exception)
                            {

                                count = 1;
                            }
                            string urlToGo = extra.gra(command, "<param name='url'>(.*?)</param>");

                            urlToGo = Regex.Replace(urlToGo, "%URL%", url, RegexOptions.Singleline | RegexOptions.IgnoreCase);


                            #region scriptparam process
                            xpath = Regex.Replace(xpath, "%script_param%", script_param, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                            id = Regex.Replace(id, "%script_param%", script_param, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                            regex = Regex.Replace(regex, "%script_param%", script_param, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                            script = Regex.Replace(script, "%script_param%", script_param, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                            text = Regex.Replace(text, "%script_param%", script_param, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                            urlToGo = Regex.Replace(urlToGo, "%script_param%", script_param, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                            picturename = Regex.Replace(picturename, "%script_param%", script_param, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                            #endregion




                            int seconds = 0;
                            int n = 0;
                            try
                            {
                                seconds = Convert.ToInt32(extra.gra(command, "<param name='seconds'>(.*?)</param>"));
                            }
                            catch
                            {

                            }
                            try
                            {
                                n = Convert.ToInt32(extra.gra(command, "<param name='n'>(.*?)</param>"));
                            }
                            catch
                            {

                            }
                            string type1 = extra.gra(command, "<type>(.*?)</type>");

                            switch (type1)
                            {
                                case "screenShot":
                                    browser.screenShot(xpath, out error, picturename);
                                    break;
                                case "elementClick":
                                    browser.eC(xpath, out error);
                                    break;
                                case "elemNeededClick":
                                    browser.eNC(xpath, out error, regex);
                                    break;
                                case "elementTypeText":
                                    browser.eTT(xpath, text, out error, true);
                                    break;
                                case "elementTypeTextAlt":
                                    browser.eTTAlt(xpath, text, out error, true);
                                    break;
                                case "executeJS":
                                    browser.js(script, out error);
                                    break;
                                case "sleep":
                                    browser.sl(seconds);
                                    break;
                                case "waitElementLoaded":
                                    browser.wEL(xpath, seconds);
                                    break;
                                case "waitNElementsLoaded":
                                    browser.wNEL(xpath, n, seconds);
                                    break;
                                case "navigate":
                                    browser.n(urlToGo, out content, out error);
                                    break;
                                case "downkey":
                                    browser.eScrollByDownKey(count, seconds, out error);
                                    break;
                                case "switchframe":
                                    browser.SwitchtoFrame(xpath, out error);
                                    break;
                                case "switchbaseframe":
                                    browser.SwitchtoBaseFrame(out error);
                                    break;
                                case "scrollpixelup":
                                    browser.scrollPixelUp(count, out error);
                                    break;
                                case "scrollpixeldown":
                                    browser.scrollPixelDown(count, out error);
                                    break;
                                case "avito":
                                    browser.n(urlToGo, out content, out error);

                                    if (browser.eE("//a[@class='button-text action-link link']"))
                                    {
                                        browser.eC("//a[@class='button-text action-link link']", out error);

                                        browser.wEL("//ul[@class='person-actions']/li[@class='person-action button-blue action-show-number  button-green']/a[@class='button-text action-link link']", 3);

                                    }

                                    break;
                                default:
                                    throw new Exception("Команда " + type1 + " не поддерживается");
                                    break;
                            }

                            if (error != "" && detectErrors)
                            {
                                throw new Exception("Команда " + type1 + " (xpath = " + xpath + " ; regex = " + regex + " ) вызвала ошибку: " + error);
                            }

                        }
                    }

                    #endregion

                    content = browser.gPC(out error);

                    #region check whether proxy banned and close if it is
                    bundle.cInvC(content, index, out error);
                    #endregion

                    if (valids != null)
                    {
                        foreach (string r in valids)
                        {
                            if (!Regex.IsMatch(content, r, RegexOptions.Singleline | RegexOptions.IgnoreCase))
                            {
                                throw new Exception("Страница, загруженная браузером, не соответствует regex валидности: " + r);
                            }
                        }
                    }

                    if (invalids != null)
                    {
                        foreach (string r in invalids)
                        {
                            if (Regex.IsMatch(content, r, RegexOptions.Singleline | RegexOptions.IgnoreCase))
                            {
                                throw new Exception("Страница, загруженная браузером, соответствует regex невалидности: " + r);
                            }
                        }
                    }
                    if (Regex.IsMatch(url, "-ID[^\\.]*?.html"))
                    {

                        try { browser.driver.FindElement(By.CssSelector("button.cookie-close")).Click(); }
                        catch { }


                        for (int w = 0; w < 5; w++)
                        {
                            int x = GetRandom(200, 500) / 15;
                            for (int q = 1; q < 16; q++)
                            { ((IJavaScriptExecutor)browser.driver).ExecuteScript("window.scrollBy(0," + x * q + ")"); System.Threading.Thread.Sleep(50); }

                            System.Threading.Thread.Sleep(200);

                            //System.Threading.Thread.Sleep(GetRandom(1000, 2500));

                            x = -(GetRandom(200, 500) / 15);
                            for (int q = 1; q < 16; q++)
                            { ((IJavaScriptExecutor)browser.driver).ExecuteScript("window.scrollBy(0," + x * q + ")"); System.Threading.Thread.Sleep(50); }
                            System.Threading.Thread.Sleep(200);
                        }


                        List<IWebElement> Checked = new List<IWebElement> { };
                        try { Checked.Add(browser.driver.FindElement(By.CssSelector("div.contact-button span"))); }
                        catch { }

                        if (Checked.Count == 1)
                        {
                        GO:
                            Thread.Sleep(100);
                            browser.driver.FindElement(By.CssSelector("div.contact-button")).Click();
                            if (browser.driver.FindElement(By.CssSelector("div.contact-button")).Text.IndexOf("x") != -1)
                                goto GO;
                        }


                    }
                    content = browser.gPC(out error);
                    return content;
                }
                #endregion

                #region data_area_gather_plugin (плагин сбора диапазонов с данными)
                if (extra.sc(type, "data_area_gather_plugin"))
                {
                    List<string> resultingList = new List<string>();

                    //параметр ССЫЛКА на страницу, на которой происходит сбор диапазонов
                    string url = parameters["url"].ToString();
                    //параметр реферер для страницы, на которой происходит сбор диапазонов
                    string referer = parameters["referer"].ToString();
                    //параметр исходный код реферера для страницы, на которой происходит сбор диапазонов
                    string referer_pagecode = parameters["referer_pagecode"].ToString();
                    //контент страницы, на которой происходит сбор диапазонов
                    string content = parameters["content"].ToString();



                    resultingList.Add("pagecode_");

                    //возвращает список диапазонов для сбора данных
                    return resultingList;
                }
                #endregion

                #region data_gather_plugin (плагин сбора данных (или плагин обработки собранных данных))
                if (extra.sc(type, "data_gather_plugin"))
                {
                    //параметр найденное значение поля данных, обработка которого производится (в него также 
                    //может быть передан исходный код диапазона или страницы, чтобы там был проведен поиск самого значения
                    string value = parameters["value"].ToString();
                    //параметр URL текущей страницы
                    string url = parameters["url"].ToString();
                    //параметр реферер текущей страницы
                    string referer = parameters["referer"].ToString();
                    //параметр исходный код реферера текущей страницы
                    string referer_pagecode = parameters["referer_pagecode"].ToString();
                    //параметр имя текущего поля данных
                    string fieldname = parameters["fieldname"].ToString();
                    //параметр ряд данных, в котором передаются значения текущей группы данных (заполнены только значения полей
                    //расположенных в списке полей выше, чем текущее поле)
                    DataRow dr = (DataRow)parameters["datarow"];

                    string result = value + "_" + "";

                    //возвращает обработанное/найденное значение поля
                    return result;// extra.gra(value, "<title>(.*?)</title>");//dr["url"]+value;
                }
                #endregion

                #region load_files_plugin (плагин загрузки файлов)
                if (extra.sc(type, "load_files_plugin"))
                {
                    List<string> resultingList = new List<string>();

                    //параметр найденное значение поля данных
                    string value = parameters["value"].ToString();
                    //параметр URL текущей страницы
                    string url = parameters["url"].ToString();
                    //параметр имя текущего поля данных
                    string fieldname = parameters["fieldname"].ToString();
                    //параметр папка для сохранения файлов
                    string folder = parameters["folder"].ToString();
                    //параметр ряд данных, в котором передаются значения текущей группы данных (заполнены только значения полей
                    //расположенных в списке полей выше, чем текущее поле)
                    DataRow dr = (DataRow)parameters["datarow"];

                    resultingList.Add("c:\\Users\\kolchakA\\Documents\\instylefashion\\tz.txt");

                    //возвращает список локальных путей загруженных файлов
                    return resultingList;//dr["url"]+value;
                }
                #endregion

                #region links_gather_plugin (плагин сбора ссылок)
                if (extra.sc(type, "links_gather_plugin"))
                {
                    HashSet<string> resultingList = new HashSet<string>();

                    //параметр ССЫЛКА на страницу, на которой производится сбор ссылок
                    string url = parameters["url"].ToString();
                    //параметр уровень вложенности страницы, на которой производится сбор ссылок
                    int nestinglevel = Convert.ToInt32(parameters["nestinglevel"].ToString());
                    //параметр реферер вложенности страницы, на которой производится сбор ссылок
                    string referer = parameters["referer"].ToString();
                    //параметр контент вложенности страницы, на которой производится сбор ссылок
                    string content = parameters["content"].ToString();

                    List<string> li = extra.GetAllLinksFromPage(content, url, true);

                    if (li.Count > 0)
                    {
                        resultingList.Add(li[0]);
                    }
                    //возвращает список собранных ссылок
                    return resultingList;
                }
                #endregion

                #region pre_export_plugin (плагин преэкспорта)
                if (extra.sc(type, "pre_export_plugin"))
                {
                    //параметр URL страницы, на которой собраны данные
                    string url = parameters["url"].ToString();
                    //параметр таблица собранных данных (содержит все собранные с текущей страницы группы данных)
                    DataTable dt = (DataTable)parameters["datatable"];
                    //параметр список названий колонок таблицы собранных данных
                    List<string> columnNames = (List<string>)parameters["columnnames"];

                    //...
                    //...
                    //...

                    //Возвращает флаг возможности экспорта собранных данных (если возвращается false, экспорт не будет проивзодиться)
                    return false;
                }
                #endregion

                #region export_plugin (плагин экспорта)
                if (extra.sc(type, "export_plugin"))
                {
                    //параметр URL страницы, на которой собраны данные
                    string url = parameters["url"].ToString();
                    //параметр конфигурация плагина экспорта
                    string config = parameters["config"].ToString();
                    //параметр таблица собранных данных (содержит все собранные с текущей страницы группы данных)
                    DataTable dt = (DataTable)parameters["datatable"];
                    //параметр список названий колонок таблицы собранных данных
                    List<string> columnNames = (List<string>)parameters["columnnames"];

                    string text = "";

                    foreach (DataRow dr in dt.Rows)
                    {
                        text = dr["content"].ToString();

                        foreach (string columnName in columnNames)
                        {
                            text = text.Replace("%" + columnName + "%", dr[columnName].ToString());
                        }
                        try
                        {
                            extra.WriteToFile(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments), "testfile.txt"),
                                    "\r\n====url = " + url + "==config=" + config + "==\r\n" + text + "\r\n========\r\n");
                        }
                        catch
                        {

                        }
                    }
                    //возвращаемое значение не используется
                }
                #endregion

                #region finalize_plugin (плагин финализации)
                if (extra.sc(type, "finalize_plugin"))
                {
                    //string index = campaignname + "_" + Thread.CurrentThread.ManagedThreadId;

                    bundle.fI(campaignname, out error);

                    //возвращаемое значение не используется
                }
                #endregion

                #region proxy checker plugins

                #region proxy_source_load_page_plugin (плагин загрузки страницы источника прокси)
                if (extra.sc(type, "proxy_source_load_page_plugin"))
                {
                    //параметр ССЫЛКА на загружаемую страницу
                    string url = parameters["url"].ToString();

                    DatacolHttp http = (DatacolHttp)parameters["datacolhttp"];
                    //параметр имя прокси чекера
                    string config = parameters["config"].ToString();


                    string index = "";
                    string content = "";

                    #region Initialize browser

                    #region plugin specific params
                    string dllpath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    string filename = Path.Combine(dllpath, "D5Plugin_Chrome.txt");
                    Dictionary<string, object> configParams = GetDictionaryParams(filename);

                    string give_control_to_user_xpath = "";
                    if (configParams.ContainsKey("give_control_to_user_xpath"))
                    {
                        give_control_to_user_xpath = configParams["give_control_to_user_xpath"].ToString();
                    }
                    #endregion


                    s browser = bundle.iI(dllpath, campaignname, configParams, out error, out index, null, false);

                    if (error != "") return "";
                    #endregion

                    browser.n(url, out content, out error);

                    if (error != "") return "";
                    //--------------
                    //------
                    //--------------

                    if (give_control_to_user_xpath != "")
                    {
                        if (browser.eE(give_control_to_user_xpath))
                        {
                            datacol_help_forms.form_interface fi = new datacol_help_forms.form_interface();
                            fi.showDialog();

                        }
                    }

                    content = browser.gPC(out error);

                    return content;
                }
                #endregion


                #region proxy_checker_finalize_plugin (плагин финализации)
                if (extra.sc(type, "proxy_checker_finalize_plugin"))
                {
                    //string index = campaignname + "_" + Thread.CurrentThread.ManagedThreadId;

                    bundle.fI(campaignname, out error);

                    //возвращаемое значение не используется
                }
                #endregion
                #endregion

            }
            catch (Exception exp)
            {
                error = exp.Message;
            }

            return "возвращаемое значение по умолчанию (для типов плагинов, у которых оно не используется)";
        }

        #region Служебные функции

        static int GetRandom(int x, int y)
        {
            Random rnd = new Random();
            int value = rnd.Next(x, y);
            return value;
        }
        public static Dictionary<string, object> GetDictionaryParamsConfig(string config)
        {

            string filecontent = config;
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            try
            {
                MatchCollection parameters = Regex.Matches(filecontent, "<dc5par[^<>]*?type=\"([^<>]*?)\"[^<>]*?name=\"([^<>]*?)\"[^<>]*?>(.*?)</dc5par>", RegexOptions.Singleline | RegexOptions.IgnoreCase);

                string type = string.Empty;
                string name = string.Empty;
                string value = string.Empty;

                int paramInt = -1;

                List<string> paramList = new List<string>();

                foreach (Match param1 in parameters)
                {


                    type = param1.Groups[1].Value.Trim();
                    name = param1.Groups[2].Value.Trim();
                    value = param1.Groups[3].Value.Trim();
                    if (type == "list-string")
                    {

                        paramList = extra.GetAllLines(value, true);

                        dictParams.Add(name, paramList);
                    }
                    else if (type == "string")
                    {
                        dictParams.Add(name, value);
                    }
                    else if (type == "int")
                    {
                        paramInt = Convert.ToInt32(value);
                        dictParams.Add(name, paramInt);
                    }
                    else
                    {
                        throw new Exception("Тип параметра " + type + " в конфигурации не поддерживается");
                    }

                }

            }
            catch
            {

            }

            return dictParams;
        }



        public static Dictionary<string, object> GetDictionaryParams(string filename, string encoding = "")
        {
            Encoding encode = null;

            if (encoding == "")
            {
                encode = Encoding.Default;
            }
            else
            {
                encode = Encoding.GetEncoding(encoding);
            }
            string filecontent = string.Empty;
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            if (!File.Exists(filename))
            {
                return dictParams;
            }
            else
            {
                filecontent = File.ReadAllText(filename, encode);

                MatchCollection parameters = Regex.Matches(filecontent, "<dc5par[^<>]*?type=\"([^<>]*?)\"[^<>]*?name=\"([^<>]*?)\"[^<>]*?>(.*?)</dc5par>", RegexOptions.Singleline | RegexOptions.IgnoreCase);

                string type = string.Empty;
                string name = string.Empty;
                string value = string.Empty;

                int paramInt = -1;

                List<string> paramList = new List<string>();

                foreach (Match param1 in parameters)
                {


                    type = param1.Groups[1].Value.Trim();
                    name = param1.Groups[2].Value.Trim();
                    value = param1.Groups[3].Value.Trim();
                    if (type == "list-string")
                    {

                        paramList = extra.GetAllLines(value, true);

                        dictParams.Add(name, paramList);
                    }
                    else if (type == "string")
                    {
                        dictParams.Add(name, value);
                    }
                    else if (type == "int")
                    {
                        paramInt = Convert.ToInt32(value);
                        dictParams.Add(name, paramInt);
                    }
                    else
                    {
                        throw new Exception("Тип параметра " + type + " в файле конфигурации " + filename + "не поддерживается");
                    }

                }

            }
            return dictParams;
        }

        #endregion

        #region Методы и свойства необходимые, для соответствия PluginInterface (обычно не используются при создании плагина)

        public void Init()
        {
            //инициализация пока не нужна
        }

        public void Destroy()
        {
            //это тоже пока не надо
        }

        public string Name
        {
            get { return "PluginName"; }
        }

        public string Description
        {
            get { return "Описание текущего плагина"; }
        }

        #endregion
    }
}
